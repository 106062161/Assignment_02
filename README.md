# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms :
    - 有3個關卡，由最後一關為boss關。
    - 玩家可以按空白鍵使用技能(技能條為100%才能發動)。
2. Animations :
    - 在3種不同類別的怪物加入了動畫
3. Particle Systems :
    - 玩家在被怪物子彈擊中或怪物子彈擊中玩家時會有particle 效果。
4. Sound effects : 
    - 遊戲中加入了BGM。
    - 怪物或玩家死亡時，會有爆炸音效。
    - 可以在Menu控制音樂(方向鍵:上/下)
5. Leaderboard : /
6. Menu :
    - 通過滑鼠點選(直接進入遊戲或選擇關卡)

# Bonus Functions Description : 
1. BOSS - 最後一關為王關。

