var game = new Phaser.Game(900, 900, Phaser.AUTO, 'canvas')

game.global = {score: 0,enemy_die: 0, monster_die:0, level: 1, loading_skill: 100, s_bullet: 0, boss_b: 1, volume: 50, invincible : 0}
game.state.add('boot', bootState)
game.state.add('load', loadState)
game.state.add('menu', menuState)
game.state.add('level', levelState)
game.state.add('play', playState)
game.state.add('level_2', playState_2)
game.state.add('level_3', playState_3)
game.state.add('win', WinState)
game.state.add('lose', LoseState)

game.state.start('boot')