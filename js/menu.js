var menuState = {
    create: function(){
        background = game.add.tileSprite(0, 0, 900, 900, 'background')
       /*  var startLabel = game.add.text(game.width/2, game.height - 80, 'press space to start', {font: '30px Arial', fill:'#ffffff'} )
        startLabel.anchor.setTo(0.5, 0.5); */
        var play_button = game.add.sprite(game.width/2 , game.height/2 - 100, 'play_button')
        play_button.anchor.setTo(0.5, 0.5)
        
        play_button.inputEnabled = true
        /* var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR) */
        play_button.events.onInputDown.add(this.start, this)
        var level_button = game.add.sprite(game.width/2 , game.height/2 , 'level')
        level_button.anchor.setTo(0.5, 0.5)
        level_button.events.onInputDown.add(this.level_select, this)
        level_button.inputEnabled = true
        this.Volume = game.add.text(game.width/2, game.height/2 + 100,"Volume: " + game.global.volume + "%", { font: '30px Arial', fill: '#fff' });
        this.Volume.anchor.setTo(0.5, 0.5)
        this.cursor = game.input.keyboard.createCursorKeys();
        this.time = 0
        

        

    },
    update:function(){

        if(this.cursor.up.isDown){
            
            if(game.global.volume < 100 && game.time.now > this.time ){
                game.global.volume = game.global.volume + 1
                this.Volume.text = "Volume: " + game.global.volume + "%"
                this.time = this.time + 150

            }
            
        }
        if(this.cursor.down.isDown){
            if(game.global.volume > 0 && game.time.now > this.time ){
                game.global.volume = game.global.volume - 1
                this.Volume.text = "Volume: " + game.global.volume + "%"
                this.time = this.time + 150
            }
            
        }
    },
    start: function(){
        this.time = 0;
        game.state.start('play')
    },
    level_select: function(){
        this.time = 0
        game.state.start('level')
    }
}
var levelState = {
    create:function(){

        background = game.add.tileSprite(0, 0, 900, 900, 'background')

        this.text1 = game.add.text(game.width/2 - 320 , game.height/2 + 80,"LEVEL-1: ", { font: '30px Arial', fill: '#fff' });
        this.text1.anchor.setTo(0.5, 0.5)
        var level1 = game.add.sprite(game.width/2 - 200, game.height/2 + 100, 'eye')
        level1.anchor.setTo(0.5, 0.5)
        level1.inputEnabled = true
        level1.events.onInputDown.add(level_1, this)

        this.text2 = game.add.text(game.width/2 - 190, game.height/2 - 60,"LEVEL-2: ", { font: '30px Arial', fill: '#fff' });
        this.text2.anchor.setTo(0.5, 0.5)
        var level2 = game.add.sprite(game.width/2 -50, game.height/2 , 'monster')
        level2.anchor.setTo(0.5, 0.5)
        level2.inputEnabled = true
        level2.events.onInputDown.add(level_2, this)

        this.text3 = game.add.text(game.width/2 + 5, game.height/2 - 180,"LEVEL-3: ", { font: '30px Arial', fill: '#fff' });
        this.text3.anchor.setTo(0.5, 0.5)
        var level3 = game.add.sprite(game.width/2  + 180, game.height/2 - 100, 'boss')
        level3.anchor.setTo(0.5, 0.5)
        level3.inputEnabled = true
        level3.events.onInputDown.add(level_3, this)


        function level_1(){
            game.state.start('play')
        }
        function level_2(){
            game.state.start('level_2')
        }
        function level_3(){
            game.state.start('level_3')
        }
    }
}