var playState = {
    preload:function(){},
    create: function(){
        background = game.add.tileSprite(0, 0, 900, 900, 'background')

        game.global.enemy_die = 0
        game.global.score = 0

        game.global.loading_skill = 100
        game.global.skill_bullet = 0;
        game.global.level = 1;
        game.global.invincble = 0
        //music
        this.bgm = game.add.audio('bgm', game.global.volume * 0.01, true);
        this.explosion = game.add.audio('explosion', game.global.volume * 0.01, false)
        this.bgm.play()
        //score
        this.ScoreString = 'Score : ';
        this.ScoreText = game.add.text(10, 10, this.ScoreString + game.global.score, { font: '30px Arial', fill: '#fff' });

        //player 
        this.player = game.add.sprite(game.width/2 , game.height/2 + 300, 'player')
        this.player.anchor.setTo(0.5, 0.5)
        this.player.health = 7;
        game.physics.enable(this.player, Phaser.Physics.ARCADE);
        this.cursor = game.input.keyboard.createCursorKeys();
        

        //enemy
        this.enemys = game.add.group()
        this.enemys.enableBody = true
        this.enemys.physicsBodyType = Phaser.Physics.ARCADE
        this.enemys.createMultiple(12, 'eye')
        game.time.events.loop(2000, this.Create_enemy, this)
        this.enemys.forEach(this.enemy_bullet, this)
        this.enemys.forEach(function(enemy){
            enemy.animations.add('fly',[0,1,2],10,true)
        })
       

        //bullet
        this.bullets = game.add.group()
        this.bullets.enableBody = true
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.bullets.createMultiple(100, 'bullet');
        game.time.events.loop(120, this.bulletProduct, this)


        //skill_bullet
        this.skill_bullets = game.add.group()
        this.skill_bullets.enableBody = true
        this.skill_bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.skill_bullets.createMultiple(100, 'skill_bullet');

        //enemy_bullet
        this.enemy_bullets = game.add.group()
        this.enemy_bullets.enableBody = true
        this.enemy_bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.enemy_bullets.createMultiple(1000, 'enemy_bullet');


        //emitter
        this.emitter = game.add.emitter (0, 0, 80)
        this.emitter.makeParticles('pixel')
        this.emitter.setYSpeed(-150, 150)
        this.emitter.setXSpeed(-150, 150)
        this.emitter.setScale(2, -2, 2, -2, 1600)
        this.emitter.gravity = 0

        //fire_emitter
        this.fire_emitter = game.add.emitter(0, 0, 100)
        this.fire_emitter.makeParticles('fire')
        this.fire_emitter.setYSpeed(-180, 180)
        this.fire_emitter.setXSpeed(-180, 180)
        this.fire_emitter.setScale(3.5, -3.5, 3.5, -3.5, 800)
        this.fire_emitter.gravity = 0
        
        //health
        this.healths = game.add.group()
        for (var i = 0; i < 5; i++) 
        {
            var health = this.healths.create(game.world.width - 230 + (50 * i), 40, 'health')
            health.anchor.setTo(0.5, 0.5)
            health.alpha = 0.4
        }

        //skill_bar
        this.skill_bar = game.add.group()
        for(var j = 0; j < 4; j++){
            var skill = this.skill_bar.create(200 - 50 * j + 50, game.world.height - 40, 'skill_bar')
            skill.anchor.setTo(0.5, 0.5)
            skill.alpha = 0.7
        }
        game.time.events.loop(120, this.restore, this)
        this.percent_bar =  game.add.text(40, game.world.height - 37, game.global.loading_skill + "%" ,{ font: '24px Arial', fill: '#fff' });
        this.percent_bar.anchor.setTo(0.5, 0.5)

        
    },
    update: function(){
        this.movePlayer();
        
        background.tilePosition.y += 1.5;
        if(this.player.alive){
            
            game.physics.arcade.overlap(this.player, this.enemy_bullets, this.enemy_damage, null, this)  
            game.physics.arcade.overlap(this.enemys, this.bullets, this.player_damage,null, this)
            game.physics.arcade.overlap(this.enemys, this.skill_bullets, this.player_damage,null, this)
            game.physics.arcade.overlap(this.enemys, this.player, this.damage, null, this) 
        }
        
        

      
    },
    damage:function(enemy, player){
        if(game.time.now > game.global.invincble && (enemy == this.player || player == this.player)){
            console.log('123')
            this.player.health = this.player.health - 1
            health = this.healths.getFirstAlive();
            if(health) health.kill()
            game.global.invincble = game.time.now + 1500
            if(this.player.health  <= 0){
                this.explosion.play()
                this.player.kill();
                game.state.start('lose')
            } 
        }
    },
    movePlayer: function(){
       
        if(this.cursor.left.isDown && this.player.x > 0){
            this.player.x -= 7
            background.tilePosition.x -= 0.5
        }
        if(this.cursor.right.isDown && this.player.x < game.world.width){
            this.player.x +=  7
            background.tilePosition.x += 0.5
        }
        if(this.cursor.up.isDown && this.player.y > 0){
             this.player.y -= 7
        }
        if(this.cursor.down.isDown && this.player.y < game.world.height){
            this.player.y += 7
        }
        if(game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).isDown){
            
            if(game.global.loading_skill == 100){
                this.skill_bar.forEach(function(skill){
                    skill.kill();
                })
                game.global.loading_skill = 0
                game.global.skill_bullet = 0
                game.time.events.loop(100, this.skill_bulletProduct, this)
            }
        }
    },
    bulletProduct:function(){
        if(this.player.alive == true){
            var bullet = this.bullets.getFirstDead()
            if(bullet){
                bullet.anchor.setTo(1, 0.5)
                bullet.reset(this.player.x + 17, this.player.y - 40)
                bullet.body.velocity.y = -850;
                bullet.checkWorldBounds = true
                bullet.outOfBoundsKill = true
            }
        }
       
    },
    skill_bulletProduct:function(){
        if(game.global.skill_bullet <= 30){
            var skill_bullet = this.skill_bullets.getFirstDead()
                if(skill_bullet){
                    game.global.skill_bullet = game.global.skill_bullet + 1;
                    skill_bullet.anchor.setTo(1, 0.5)
                    skill_bullet.reset(this.player.x + 17, this.player.y - 40)
                    skill_bullet.body.velocity.y = -500;
                    skill_bullet.body.velocity.x = game.rnd.integerInRange(-350, 350)   
                    skill_bullet.checkWorldBounds = true
                    skill_bullet.outOfBoundsKill = true
                }
        }
        
    },
    Create_enemy:function(){
        console.log('create')
        if( game.global.enemy_die < 11){
            var enemy = this.enemys.getFirstDead()
        
            var first = 1
            if(enemy){
                enemy.anchor.setTo(0.5, 0.5)
                enemy.reset(-40, 20)
                enemy.health = 7
                enemy.play('fly')
           
                game.time.events.add(780, enemy_Move, this )
  
            }
        
            function enemy_Move(){
                tween = game.add.tween(enemy).to({x:game.rnd.integerInRange(700, 1200), y: game.rnd.integerInRange(25, 350)}, 3000).start()
                tween.onComplete.add(onComplete, this)
            }
            function onComplete() {
                if(first == 1) game.add.tween(enemy).to({x:game.rnd.integerInRange(20, 100), y: game.rnd.integerInRange(25, 350)}, game.rnd.integerInRange(2400, 3000)).yoyo(true).start()
                game.time.events.loop(6000, enemy_Action, this)
            }
            function enemy_Action(){
                first = 0;
                game.add.tween(enemy).to({x: game.rnd.integerInRange(20, 100), y: game.rnd.integerInRange(25, 350)},game.rnd.integerInRange(2400, 3000)).yoyo(true).start()
          
            }
        }
        
        
       
    },
    enemy_bullet:function(enemy){
        game.time.events.loop(600, enemy_bullet_Product, this)
        function enemy_bullet_Product(){
            if(enemy.alive == true){
                var enemy_bullet = this.enemy_bullets.getFirstDead()
                if(enemy_bullet){
                    enemy_bullet.anchor.setTo(1, 0.5)
                    enemy_bullet.reset(enemy.x, enemy.y + 40)
                    enemy_bullet.body.velocity.y = 250
                    enemy_bullet.body.velocity.x = game.rnd.integerInRange(-100, 100)
                    enemy_bullet.angle = 300
                    enemy_bullet.checkWorldBounds = true
                    enemy_bullet.outOfBoundsKill = true
                }
            }
        }
    },
    player_damage:function(enemy, bullet){
        console.log(bullet.anchor.x, enemy.anchor.x)
        /* console.log( enemy.health ) */;
        
        if(bullet.alive && enemy.alive && bullet.anchor.x != enemy.anchor.x ){
            bullet.kill()
            enemy.health = enemy.health - 1;
            this.emitter.x = enemy.x
            this.emitter.y = enemy.y
            this.emitter.start(true, 600, null, 60)
            if(enemy.health <= 0) {
                this.explosion.play()
                enemy.kill();
                this.enemys.remove(enemy)
                game.global.enemy_die =  game.global.enemy_die + 1
                game.global.score = game.global.score + 100
                this.ScoreText.text = this.ScoreString + game.global.score
                if(game.global.enemy_die == 10){
                    game.global.enemy_die = 0;
                    game.state.start('win')
                }
            }
        }
        
        
        
       
        
    },
    enemy_damage: function(player, bullet){
        if(player.alive && bullet.alive && game.time.now > game.global.invincble){
            this.fire_emitter.x = player.x
            this.fire_emitter.y = player.y
            this.fire_emitter.start(true, 400, null, 100)
            game.global.invincble = game.time.now + 1500

            this.player.health = this.player.health - 1;
            health = this.healths.getFirstAlive();
            if(health) health.kill()
            bullet.kill();
            if(this.player.health  <= 0){
                this.explosion.play()
                this.player.kill();
                game.state.start('lose')
            } 
        }
    },        
   
    restore: function(){
        if(game.global.loading_skill < 100){
            game.global.loading_skill = game.global.loading_skill + 1
            this.percent_bar.text = game.global.loading_skill + "%"
            if(game.global.loading_skill == 25 || game.global.loading_skill == 50 || game.global.loading_skill == 75 ){
                var skill = this.skill_bar.getFirstDead()
                if(skill){
                    skill.reset((game.global.loading_skill/25 - 1) * 50 + 100 ,game.world.height - 40)
                    skill.anchor.setTo(0.5, 0.5)
                }
            }
            else if(game.global.loading_skill == 99){
                var skill = this.skill_bar.getFirstDead()
                if(skill){
                    skill.reset(250, game.world.height - 40)
                    skill.anchor.setTo(0.5, 0.5)
                }
            }
        }
        
    }
   
}














var playState_2 = {
    preload:function(){},
    create: function(){
        background = game.add.tileSprite(0, 0, 900, 900, 'background')

        game.global.enemy_die = 0
        game.global.monsters_die = 0
        

        game.global.loading_skill = 100
        game.global.skill_bullet = 0;
        game.global.level = 2;
        game.global.invincble = 0;

        //music
        this.bgm = game.add.audio('bgm', game.global.volume * 0.01, true);
        this.explosion = game.add.audio('explosion', game.global.volume * 0.01, false)
        this.bgm.play()

        //score
        this.ScoreString = 'Score : ';
        this.ScoreText = game.add.text(10, 10, this.ScoreString + game.global.score, { font: '30px Arial', fill: '#fff' });

        //player 
        this.player = game.add.sprite(game.width/2 , game.height/2 + 300, 'player')
        this.player.anchor.setTo(0.5, 0.5)
        this.player.health = 5;
        game.physics.enable(this.player, Phaser.Physics.ARCADE);
        this.cursor = game.input.keyboard.createCursorKeys();
        

        //enemy
        this.enemys = game.add.group()
        this.enemys.enableBody = true
        this.enemys.physicsBodyType = Phaser.Physics.ARCADE
        this.enemys.createMultiple(8, 'eye')
        game.time.events.loop(2000, this.Create_enemy, this)
        this.enemys.forEach(this.enemy_bullet, this)
        this.enemys.forEach(function(enemy){
            enemy.animations.add('fly',[0,1,2],10,true)
        })

        //monster
        this.monsters = game.add.group()
        this.monsters.enableBody = true
        this.monsters.physicsBodyType = Phaser.Physics.ARCADE
        this.monsters.createMultiple(7, 'monster')
        game.time.events.loop(2500, this.Create_monster, this)
        this.monsters.forEach(this.monster_bullet, this)
        this.monsters.forEach(function(monster){
            monster.animations.add('m_fly',[0,1,2,3,4,5],10,true)
        })
        game.time.events.loop(20, check_die, this)
        function check_die(){
            this.monsters.forEach(function(monster){
                if(monster.y > game.world.width + 10){
                    monster.kill();

                    game.global.monsters_die = game.global.monsters_die + 1
                }
            })
        }
        
        

        //bullet
        this.bullets = game.add.group()
        this.bullets.enableBody = true
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.bullets.createMultiple(100, 'bullet');
        game.time.events.loop(120, this.bulletProduct, this)


        //skill_bullet
        this.skill_bullets = game.add.group()
        this.skill_bullets.enableBody = true
        this.skill_bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.skill_bullets.createMultiple(100, 'skill_bullet');

        //enemy_bullet
        this.enemy_bullets = game.add.group()
        this.enemy_bullets.enableBody = true
        this.enemy_bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.enemy_bullets.createMultiple(1000, 'enemy_bullet');

        //monster_bullet
        this.monster_bullets = game.add.group()
        this.monster_bullets.enableBody = true
        this.monster_bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.monster_bullets.createMultiple(500, 'monster_bullet')

        //emitter
        this.emitter = game.add.emitter (0, 0, 80)
        this.emitter.makeParticles('pixel')
        this.emitter.setYSpeed(-150, 150)
        this.emitter.setXSpeed(-150, 150)
        this.emitter.setScale(2, -2, 2, -2, 1600)
        this.emitter.gravity = 0

        //fire_emitter
        this.fire_emitter = game.add.emitter(0, 0, 100)
        this.fire_emitter.makeParticles('fire')
        this.fire_emitter.setYSpeed(-180, 180)
        this.fire_emitter.setXSpeed(-180, 180)
        this.fire_emitter.setScale(3.5, -3.5, 3.5, -3.5, 800)
        this.fire_emitter.gravity = 0
        
        //health
        this.healths = game.add.group()
        for (var i = 0; i < 5; i++) 
        {
            var health = this.healths.create(game.world.width - 230 + (50 * i), 40, 'health')
            health.anchor.setTo(0.5, 0.5)
            health.alpha = 0.4
        }

        //skill_bar
        this.skill_bar = game.add.group()
        for(var j = 0; j < 4; j++){
            var skill = this.skill_bar.create(200 - 50 * j + 50, game.world.height - 40, 'skill_bar')
            skill.anchor.setTo(0.5, 0.5)
            skill.alpha = 0.7
        }
        game.time.events.loop(120, this.restore, this)
        this.percent_bar =  game.add.text(40, game.world.height - 37, game.global.loading_skill + "%" ,{ font: '24px Arial', fill: '#fff' });
        this.percent_bar.anchor.setTo(0.5, 0.5)

        
    },
    update: function(){
        this.movePlayer();
        
        background.tilePosition.y += 1.5;
        if(this.player.alive){
            
            game.physics.arcade.overlap(this.player, this.enemy_bullets, this.enemy_damage, null, this)
            game.physics.arcade.overlap(this.player, this.monster_bullets, this.enemy_damage, null, this)

            game.physics.arcade.overlap(this.enemys, this.bullets, this.player_damage,null, this)
            game.physics.arcade.overlap(this.enemys, this.skill_bullets, this.player_damage,null, this) 

            game.physics.arcade.overlap(this.monsters, this.bullets, this.player_damage_m, null, this)
            game.physics.arcade.overlap(this.monsters, this.skill_bullets, this.player_damage_m, null, this) 

            game.physics.arcade.overlap(this.monsters, this.player, this.damage, null, this) 
            game.physics.arcade.overlap(this.enemys, this.player, this.damage, null, this) 
            
            if(game.global.enemy_die >= 6 && game.global.monsters_die >= 5){
                game.global.enemy_die = 0;
                game.global.monsters_die = 0;
                game.state.start('win')
            }
            
        }
        
        

      
    },
    damage:function(enemy, player){
        if(game.time.now > game.global.invincble && (enemy == this.player || player == this.player)){
            console.log('123')
            this.player.health = this.player.health - 1
            health = this.healths.getFirstAlive();
            if(health) health.kill()
            game.global.invincble = game.time.now + 1500
            if(this.player.health  <= 0){
                this.explosion.play()
                this.player.kill();
                game.state.start('lose')
            } 
        }
    },
    movePlayer: function(){
       
        if(this.cursor.left.isDown && this.player.x > 0){
            this.player.x -= 7
            background.tilePosition.x -= 0.5
        }
        if(this.cursor.right.isDown && this.player.x < game.world.width){
            this.player.x +=  7
            background.tilePosition.x += 0.5
        }
        if(this.cursor.up.isDown && this.player.y > 0){
             this.player.y -= 7
        }
        if(this.cursor.down.isDown && this.player.y < game.world.height){
            this.player.y += 7
        }
        
        if(game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).isDown){
            
            if(game.global.loading_skill == 100){
                this.skill_bar.forEach(function(skill){
                    skill.kill();
                })
                game.global.loading_skill = 0
                game.global.skill_bullet = 0
                game.time.events.loop(100, this.skill_bulletProduct, this)
            }
        }
    },
    bulletProduct:function(){
        if(this.player.alive == true){
            var bullet = this.bullets.getFirstDead()
            if(bullet){
                bullet.anchor.setTo(1, 0.5)
                bullet.reset(this.player.x + 17, this.player.y - 40)
                bullet.body.velocity.y = -850;
                bullet.checkWorldBounds = true
                bullet.outOfBoundsKill = true
            }
        }
       
    },
    skill_bulletProduct:function(){
        if(game.global.skill_bullet <= 30){
            var skill_bullet = this.skill_bullets.getFirstDead()
                if(skill_bullet){
                    game.global.skill_bullet = game.global.skill_bullet + 1;
                    skill_bullet.anchor.setTo(1, 0.5)
                    skill_bullet.reset(this.player.x + 17, this.player.y - 40)
                    skill_bullet.body.velocity.y = -500;
                    skill_bullet.body.velocity.x = game.rnd.integerInRange(-350, 350)   
                    skill_bullet.checkWorldBounds = true
                    skill_bullet.outOfBoundsKill = true
                }
        }
        
    },
    Create_enemy:function(){
        console.log('create')
        if( game.global.enemy_die < 7){
            var enemy = this.enemys.getFirstDead()
        
            var first = 1
            if(enemy){
                enemy.anchor.setTo(0.5, 0.5)
                enemy.reset(-40, 20)
                enemy.health = 7
                enemy.play('fly')
           
                game.time.events.add(780, enemy_Move, this )
  
            }
        
            function enemy_Move(){
                tween = game.add.tween(enemy).to({x:game.rnd.integerInRange(700, 1200), y: game.rnd.integerInRange(25, 350)}, 3000).start()
                tween.onComplete.add(onComplete, this)
            }
            function onComplete() {
                if(first == 1) game.add.tween(enemy).to({x:game.rnd.integerInRange(20, 100), y: game.rnd.integerInRange(25, 350)}, game.rnd.integerInRange(2400, 3000)).yoyo(true).start()
                game.time.events.loop(6000, enemy_Action, this)
            }
            function enemy_Action(){
                first = 0;
                game.add.tween(enemy).to({x: game.rnd.integerInRange(20, 100), y: game.rnd.integerInRange(25, 350)},game.rnd.integerInRange(2400, 3000)).yoyo(true).start()
          
            }
        }
    },
    Create_monster:function(){
        if( game.global.monsters_die < 6){
            var monster = this.monsters.getFirstDead()
        
            
            if(monster){
                monster.anchor.setTo(0.5, 0.5)
                monster.reset(game.rnd.integerInRange(40, game.world.width - 40), -50)
                monster.health = 15
                monster.play('m_fly')
                monster.body.velocity.y = 100
                
  
            }
        }
    },
    enemy_bullet:function(enemy){
        game.time.events.loop(600, enemy_bullet_Product, this)
        function enemy_bullet_Product(){
            if(enemy.alive == true){
                var enemy_bullet = this.enemy_bullets.getFirstDead()
                if(enemy_bullet){
                    enemy_bullet.anchor.setTo(1, 0.5)
                    enemy_bullet.reset(enemy.x, enemy.y + 40)
                    enemy_bullet.body.velocity.y = 250
                    enemy_bullet.body.velocity.x = game.rnd.integerInRange(-100, 100)
                    enemy_bullet.angle = 300
                    enemy_bullet.checkWorldBounds = true
                    enemy_bullet.outOfBoundsKill = true
                }
            }
        }
    },
    monster_bullet:function(monster){
        game.time.events.loop(1800, monster_bullet_Product, this)
        function monster_bullet_Product(){
            if(monster.alive == true){
                for(var i = 0; i < 10; i++){
                  var monster_bullet = this.monster_bullets.getFirstDead()
                    if(monster_bullet){
                        monster_bullet.anchor.setTo(1, 0.5)
                        monster_bullet.reset(monster.x, monster.y + 40)
                        monster_bullet.body.velocity.y = 250
                        monster_bullet.body.velocity.x = game.rnd.integerInRange(-50, 50)
                    
                        monster_bullet.checkWorldBounds = true
                        monster_bullet.outOfBoundsKill = true
                    }  
                }
                
            }
        }
    },
    player_damage:function(enemy, bullet){
        console.log(bullet.anchor.x, enemy.anchor.x)
        /* console.log( enemy.health ) */;
        
        if(bullet.alive && enemy.alive && bullet.anchor.x != enemy.anchor.x ){
            bullet.kill()
            enemy.health = enemy.health - 1;
            this.emitter.x = enemy.x
            this.emitter.y = enemy.y
            this.emitter.start(true, 600, null, 60)

            if(enemy.health <= 0) {
                this.explosion.play()
                enemy.kill();
                this.enemys.remove(enemy)
                game.global.enemy_die =  game.global.enemy_die + 1
                game.global.score = game.global.score + 100
                this.ScoreText.text = this.ScoreString + game.global.score
                if(game.global.enemy_die >= 6 && game.global.monsters_die >= 5){
                    game.global.enemy_die = 0;
                    game.global.monsters_die = 0;
                    game.state.start('win')
                }
            }
        }
    },
    player_damage_m:function(monster, bullet){
        console.log('1')
        if(bullet.alive && monster.alive && bullet.anchor.x != monster.anchor.x ){
            bullet.kill()
            monster.health = monster.health - 1;
            this.emitter.x = monster.x
            this.emitter.y = monster.y
            this.emitter.start(true, 600, null, 60)
            if(monster.health <= 0) {
                this.explosion.play()
                monster.kill();
                this.monsters.remove(monster)
                game.global.monsters_die =  game.global.monsters_die + 1
                game.global.score = game.global.score + 150
                this.ScoreText.text = this.ScoreString + game.global.score
                if(game.global.enemy_die >= 6 && game.global.monsters_die >= 5){
                    game.global.enemy_die = 0;
                    game.global.monsters_die = 0;
                    game.state.start('win')
                }
            }
        }
    },
    enemy_damage: function(player, bullet ){
        if(player.alive && bullet.alive && game.time.now > game.global.invincble){
            this.fire_emitter.x = player.x
            this.fire_emitter.y = player.y
            this.fire_emitter.start(true, 400, null, 100)
            game.global.invincble = game.time.now + 1500

            this.player.health = this.player.health - 1;
            health = this.healths.getFirstAlive();
            if(health) health.kill()
            bullet.kill();
            if(this.player.health  <= 0){
                this.explosion.play()
                this.player.kill();
                game.state.start('lose')
            } 
        }
    },        
   
    restore: function(){
        if(game.global.loading_skill < 100){
            game.global.loading_skill = game.global.loading_skill + 1
            this.percent_bar.text = game.global.loading_skill + "%"
            if(game.global.loading_skill == 25 || game.global.loading_skill == 50 || game.global.loading_skill == 75 ){
                var skill = this.skill_bar.getFirstDead()
                if(skill){
                    skill.reset((game.global.loading_skill/25 - 1) * 50 + 100 ,game.world.height - 40)
                    skill.anchor.setTo(0.5, 0.5)
                }
            }
            else if(game.global.loading_skill == 99){
                var skill = this.skill_bar.getFirstDead()
                if(skill){
                    skill.reset(250, game.world.height - 40)
                    skill.anchor.setTo(0.5, 0.5)
                }
            }
        }
        
    }
   
}















var playState_3 = {
    preload:function(){},
    create: function(){
        background = game.add.tileSprite(0, 0, 900, 900, 'background')

        game.global.enemy_die = 0
        

        game.global.loading_skill = 100
        game.global.skill_bullet = 0;
        game.global.level = 3;
        game.global.boss_bullet = 0;
        game.global.invincble = 0

        //music
        this.bgm = game.add.audio('bgm', game.global.volume * 0.01, true);
        this.explosion = game.add.audio('explosion', game.global.volume * 0.01, false)
        this.bgm.play()

        //score
        this.ScoreString = 'Score : ';
        this.ScoreText = game.add.text(10, 10, this.ScoreString + game.global.score, { font: '30px Arial', fill: '#fff' });

        //player 
        this.player = game.add.sprite(game.width/2 , game.height/2 + 300, 'player')
        this.player.anchor.setTo(0.5, 0.5)
        this.player.health = 5;
        game.physics.enable(this.player, Phaser.Physics.ARCADE);
        this.cursor = game.input.keyboard.createCursorKeys();
        

        //enemy
        this.boss = game.add.sprite(game.width/2, -80, 'boss')
        this.boss.animations.add('b_fly',[0,1,2],10,true)
        this.boss.play('b_fly')
        this.boss.enableBody = true
        this.boss.anchor.setTo(0.5, 0.5)
        this.boss.health = 220
        game.physics.enable(this.boss, Phaser.Physics.ARCADE)
        this.boss.body.velocity.y = 100

        game.time.events.loop(4500, boss_action, this)

        function boss_action(){
            game.time.events.add(0, boss_down, this)
            game.time.events.add(1500, boss_stop, this)
            game.time.events.add(3000, boss_up, this)
            

        }
        

        function boss_up(){
            this.boss.body.velocity.y = 100
        }
        function boss_down(){
            this.boss.body.velocity.y = -100
        }
        function boss_stop(){
            this.boss.body.velocity.y = 0
        }

        //bullet
        this.bullets = game.add.group()
        this.bullets.enableBody = true
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.bullets.createMultiple(100, 'bullet');
        game.time.events.loop(120, this.bulletProduct, this)


        //skill_bullet
        this.skill_bullets = game.add.group()
        this.skill_bullets.enableBody = true
        this.skill_bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.skill_bullets.createMultiple(100, 'skill_bullet');

        //boss_bullet
        this.boss_bullets = game.add.group()
        this.boss_bullets.enableBody = true
        this.boss_bullets.physicsBodyType = Phaser.Physics.ARCADE
        this.boss_bullets.createMultiple(1000, 'boss_bullet');
        game.time.events.add(4500, this.boss_bullet, this)

        //emitter
        this.emitter = game.add.emitter (0, 0, 80)
        this.emitter.makeParticles('pixel')
        this.emitter.setYSpeed(-150, 150)
        this.emitter.setXSpeed(-150, 150)
        this.emitter.setScale(2, -2, 2, -2, 1600)
        this.emitter.gravity = 0

        //fire_emitter
        this.fire_emitter = game.add.emitter(0, 0, 100)
        this.fire_emitter.makeParticles('fire')
        this.fire_emitter.setYSpeed(-180, 180)
        this.fire_emitter.setXSpeed(-180, 180)
        this.fire_emitter.setScale(3.5, -3.5, 3.5, -3.5, 800)
        this.fire_emitter.gravity = 0
        
        //health
        this.healths = game.add.group()
        for (var i = 0; i < 5; i++) 
        {
            var health = this.healths.create(game.world.width - 230 + (50 * i), 40, 'health')
            health.anchor.setTo(0.5, 0.5)
            health.alpha = 0.4
        }

        //skill_bar
        this.skill_bar = game.add.group()
        for(var j = 0; j < 4; j++){
            var skill = this.skill_bar.create(200 - 50 * j + 50, game.world.height - 40, 'skill_bar')
            skill.anchor.setTo(0.5, 0.5)
            skill.alpha = 0.7
        }
        game.time.events.loop(120, this.restore, this)
        this.percent_bar =  game.add.text(40, game.world.height - 37, game.global.loading_skill + "%" ,{ font: '24px Arial', fill: '#fff' });
        this.percent_bar.anchor.setTo(0.5, 0.5)

        
    },
    update: function(){
        this.movePlayer();
        
        background.tilePosition.y += 1.5;
        if(this.player.alive){
            
            game.physics.arcade.overlap(this.player, this.boss_bullets, this.enemy_damage, null, this)  
            game.physics.arcade.overlap(this.player, this.boss, this.damage, null, this)
            game.physics.arcade.overlap(this.boss, this.bullets, this.player_damage,null, this)
            game.physics.arcade.overlap(this.boss, this.skill_bullets, this.player_damage,null, this)       
            
        }
        
        

      
    },
    damage:function(){
        if(game.time.now > game.global.invincble){
            console.log('123')
            this.player.health = this.player.health - 1
            health = this.healths.getFirstAlive();
            if(health) health.kill()
            game.global.invincble = game.time.now + 1500
            if(this.player.health  <= 0){
                this.explosion.play()
                this.player.kill();
                game.state.start('lose')
            } 
        }
    },
    movePlayer: function(){
       
        if(this.cursor.left.isDown && this.player.x > 0){
            this.player.x -= 7
            background.tilePosition.x -= 0.5
        }
        if(this.cursor.right.isDown && this.player.x < game.world.width){
            this.player.x +=  7
            background.tilePosition.x += 0.5
        }
        if(this.cursor.up.isDown && this.player.y > 0){
             this.player.y -= 7
        }
        if(this.cursor.down.isDown && this.player.y < game.world.height){
            this.player.y += 7
        }
        
        if(game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).isDown){
            
            if(game.global.loading_skill == 100){
                this.skill_bar.forEach(function(skill){
                    skill.kill();
                })
                game.global.loading_skill = 0
                game.global.skill_bullet = 0
                game.time.events.loop(100, this.skill_bulletProduct, this)
            }
        }
    },
    bulletProduct:function(){
        if(this.player.alive == true){
            var bullet = this.bullets.getFirstDead()
            if(bullet){
                bullet.anchor.setTo(1, 0.5)
                bullet.reset(this.player.x + 17, this.player.y - 40)
                bullet.body.velocity.y = -850;
                bullet.checkWorldBounds = true
                bullet.outOfBoundsKill = true
            }
        }
       
    },
    skill_bulletProduct:function(){
        if(game.global.skill_bullet <= 30){
            var skill_bullet = this.skill_bullets.getFirstDead()
                if(skill_bullet){
                    game.global.skill_bullet = game.global.skill_bullet + 1;
                    skill_bullet.anchor.setTo(1, 0.5)
                    skill_bullet.reset(this.player.x + 17, this.player.y - 40)
                    skill_bullet.body.velocity.y = -500;
                    skill_bullet.body.velocity.x = game.rnd.integerInRange(-350, 350)   
                    skill_bullet.checkWorldBounds = true
                    skill_bullet.outOfBoundsKill = true
                }
        }
        
    },
    Create_enemy:function(){
        console.log('create')
        if( game.global.enemy_die < 11){
            var enemy = this.enemys.getFirstDead()
        
            var first = 1
            if(enemy){
                enemy.anchor.setTo(0.5, 0.5)
                enemy.reset(-40, 20)
                enemy.health = 5
                enemy.play('fly')
           
                game.time.events.add(780, enemy_Move, this )
  
            }
        
            function enemy_Move(){
                tween = game.add.tween(enemy).to({x:game.rnd.integerInRange(700, 1200), y: game.rnd.integerInRange(25, 350)}, 3000).start()
                tween.onComplete.add(onComplete, this)
            }
            function onComplete() {
                if(first == 1) game.add.tween(enemy).to({x:game.rnd.integerInRange(20, 100), y: game.rnd.integerInRange(25, 350)}, game.rnd.integerInRange(2400, 3000)).yoyo(true).start()
                game.time.events.loop(6000, enemy_Action, this)
            }
            function enemy_Action(){
                first = 0;
                game.add.tween(enemy).to({x: game.rnd.integerInRange(20, 100), y: game.rnd.integerInRange(25, 350)},game.rnd.integerInRange(2400, 3000)).yoyo(true).start()
          
            }
        }
        
        
       
    },
    boss_bullet:function(){
        
        game.time.events.loop(230, boss_bullet_Product, this)
        function boss_bullet_Product(){
            if(this.boss.alive == true){
                game.global.boss_bullet = game.global.boss_bullet + 1
                
                var boss_bullet = this.boss_bullets.getFirstDead()

                if(this.boss.health > 100){
                    
                    if(game.global.boss_bullet ==  20){
                        game.global.boss_bullet = 0;
                        game.global.boss_b = game.global.boss_b * - 1
                     } 
                    if(boss_bullet){
                        boss_bullet.anchor.setTo(1, 0.5)
                        boss_bullet.reset(this.boss.x, this.boss.y + 40)
                    
                        boss_bullet.body.velocity.y = 320
                        if(game.global.boss_b == 1)
                            boss_bullet.body.velocity.x =  -300 + 30 * game.global.boss_bullet
                        else
                            boss_bullet.body.velocity.x =  300 - 30 * game.global.boss_bullet

                        boss_bullet.angle = 300
                        boss_bullet.checkWorldBounds = true
                        boss_bullet.outOfBoundsKill = true
                    }
                }
                else{
                    if(boss_bullet){
                        if(game.global.boss_bullet ==  30){
                            game.global.boss_bullet = 0;
                            game.global.boss_b = game.global.boss_b * - 1
                         } 
                        boss_bullet.anchor.setTo(1, 0.5)
                        boss_bullet.reset(this.boss.x, this.boss.y + 40)
                    
                        boss_bullet.body.velocity.y = 320
                        if(game.global.boss_b == 1){
                            boss_bullet.body.velocity.x =  -600 + 40 * game.global.boss_bullet
                            boss_bullet.body.velocity.y = 300  - 10 * game.global.boss_bullet
                        }
                        else{
                            boss_bullet.body.velocity.x =  600 - 40 * game.global.boss_bullet
                            boss_bullet.body.velocity.y = - 300 + 10 * game.global.boss_bullet
                        }
                            

                        boss_bullet.angle = 300
                        boss_bullet.checkWorldBounds = true
                        boss_bullet.outOfBoundsKill = true
                    }
                }
            }
                
        }
    },
    player_damage:function(enemy, bullet){
        console.log(bullet.anchor.x, enemy.anchor.x)
        /* console.log( enemy.health ) */;
        console.log(this.boss.health)
        
        if(bullet.alive && enemy.alive && bullet.anchor.x != enemy.anchor.x ){
            bullet.kill()
            enemy.health = enemy.health - 1;
            this.emitter.x = enemy.x
            this.emitter.y = enemy.y
            this.emitter.start(true, 600, null, 60)
            if(enemy.health <= 0) {
                enemy.kill();
                this.explosion.play()
                
                game.global.score = game.global.score + 1000
                this.ScoreText.text = this.ScoreString + game.global.score
                
                game.state.start('win')
                
            }
        }
        
        
        
       
        
    },
    enemy_damage: function(player, bullet){
        if(player.alive && bullet.alive && game.time.now > game.global.invincble ){
            this.fire_emitter.x = player.x
            this.fire_emitter.y = player.y
            this.fire_emitter.start(true, 400, null, 100)
            game.global.invincble = game.time.now + 1500

            this.player.health = this.player.health - 1;
            health = this.healths.getFirstAlive();
            if(health) health.kill()
            bullet.kill();
            if(this.player.health  <= 0){
                this.explosion.play()
                this.player.kill();
                game.state.start('lose')
            } 
        }
    },        
   
    restore: function(){
        if(game.global.loading_skill < 100){
            game.global.loading_skill = game.global.loading_skill + 1
            this.percent_bar.text = game.global.loading_skill + "%"
            if(game.global.loading_skill == 25 || game.global.loading_skill == 50 || game.global.loading_skill == 75 ){
                var skill = this.skill_bar.getFirstDead()
                if(skill){
                    skill.reset((game.global.loading_skill/25 - 1) * 50 + 100 ,game.world.height - 40)
                    skill.anchor.setTo(0.5, 0.5)
                }
            }
            else if(game.global.loading_skill == 99){
                var skill = this.skill_bar.getFirstDead()
                if(skill){
                    skill.reset(250, game.world.height - 40)
                    skill.anchor.setTo(0.5, 0.5)
                }
            }
        }
        
    }
   
}
var WinState = {
    preload:function(){},
    create:function(){
        background = game.add.tileSprite(0, 0, 900, 900, 'background')
        ScoreString = 'Score : '
        this.winText = game.add.text(game.width/2, game.height/2 - 100,'You Win', { font: '60px Arial', fill: '#fff' })
        this.winText.anchor.setTo(0.5, 0.5)
        this.Score = game.add.text(game.width/2, game.height/2 , ScoreString + game.global.score, { font: '60px Arial', fill: '#fff' })
        this.Score.anchor.setTo(0.5, 0.5)
        this.Text = game.add.text(game.width/2, game.height/2 + 100,'Press SpaceBar to continue', { font: '45px Arial', fill: '#fff' })
        this.Text.anchor.setTo(0.5, 0.5)
        console.log(game.global.level)
        if(game.global.level == 3){
            this.Text.text = "Press SpaceBar back to menu"

        } 
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        upKey.onDown.add(this.start , this);
    },
    start:function(){
        console.log("1")
        if(game.global.level == 1){
            game.state.start('level_2')
            game.global.level = 2
        }
        else if(game.global.level == 2){
            game.state.start('level_3')
            game.global.level = 3;
            
        }
        else if(game.global.level == 3){
            game.state.start('menu')
            game.global.score = 0;
            game.global.level = 1
        }
    }
}
var LoseState = {
    preload:function(){},
    create:function(){
        background = game.add.tileSprite(0, 0, 900, 900, 'background')
        ScoreString = 'Score : '
        this.winText = game.add.text(game.width/2, game.height/2 - 100,'You Lose', { font: '60px Arial', fill: '#fff' })
        this.winText.anchor.setTo(0.5, 0.5)
        this.Score = game.add.text(game.width/2, game.height/2 , ScoreString + game.global.score, { font: '60px Arial', fill: '#fff' })
        this.Score.anchor.setTo(0.5, 0.5)
        this.Text = game.add.text(game.width/2, game.height/2 + 100,'Press SpaceBar back to menu', { font: '45px Arial', fill: '#fff' })
        this.Text.anchor.setTo(0.5, 0.5)
        console.log(game.global.level)
    
            

        
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
        upKey.onDown.add(this.start , this);
    },
    start:function(){
        console.log('1')
        game.global.level = 1;
        game.global.score = 0;
        game.state.start('menu')
            

    }
}

