var loadState = {
    preload: function(){
        //var progessBar = game.add.sprite(game.width/2, 200, 'progressBar')
        //prgressBar.anchor.setTo(0.5, 0.5);
        //game.load.setProloadSprite(progessBar)

        game.load.image('background', './assets/background.png')
        game.load.image('play_button', './assets/play_button.png')
        game.load.image('level', './assets/level.png')

        game.load.image('player', './assets/shooter.png')
        
        game.load.image('health', './assets/health.png')
        game.load.image('bullet', './assets/bullet.png')
        game.load.image('enemy_bullet','./assets/enemy_bullet.png')
        game.load.image('monster_bullet','./assets/monster_bullet.png')
        game.load.image('skill_bullet', './assets/skill_bullet.png')
        game.load.image('skill_bar', './assets/skill.png')
        game.load.image('boss_bullet','./assets/boss_bullet.png')
        
        game.load.image('pixel', './assets/pixel.png')
        game.load.image('fire', './assets/fire.png')

        
        game.load.spritesheet('boss', './assets/boss.png' ,264, 190) 
        game.load.spritesheet('eye', './assets/eye_sprite.png', 83, 121)
        game.load.spritesheet('monster', './assets/monster_2.png', 148, 136)



        game.load.audio('bgm', 'assets/bgm.mp3');
        game.load.audio('explosion', 'assets/explosion.mp3');

        
        
    },
    create: function(){
        game.state.start('menu')
    }

}